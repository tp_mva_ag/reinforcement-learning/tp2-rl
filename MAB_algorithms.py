from arms import *
import matplotlib.pyplot as plt
from tqdm import tqdm


def ucb1(t_max, mab, rho=None):
    """ Apply the UCB1 algorithm to estimate the arms

    :param t_max: Number of iteration
    :param mab: Multi-arm bandit
    :param rho: list of rho_t parameters
    :return: rew: list of all the rewards
             draws : list of the pulled arms
    """
    # Initialisations
    if rho is None:
        rho = []
    rew = []
    draws = []
    n = np.zeros(len(mab))
    s = np.zeros(len(mab))
    means = np.zeros(len(mab))

    # First pulls of the K arms
    for i in range(len(mab)):
        reward = mab[i].sample()
        draws.append(i)
        rew.append(reward)
        n[i] += 1
        s[i] += reward
        means[i] = s[i] / n[i]

    # Pulling the arms to get to t_max
    while len(draws) < t_max:
        t = len(draws)
        try:
            rho_t = rho[t]
        except IndexError:
            rho_t = 1

        chos_arm = np.argmax(means + rho_t * np.sqrt(np.log(t) / (2 * n)))

        reward = mab[chos_arm].sample()
        draws.append(chos_arm)
        rew.append(reward)
        n[chos_arm] += 1
        s[chos_arm] += reward
        means[chos_arm] = s[chos_arm] / n[chos_arm]

    return rew, draws


def ts(t_max, mab):
    """ Apply the TS algorithm to estimate the arms

    :param t_max: Number of iteration
    :param mab: Multi-arm bandit
    :return: rew: list of all the rewards
             draws : list of the pulled arms
    """
    # Initialisations
    rew = []
    draws = []
    n = np.zeros(len(mab))
    s = np.zeros(len(mab))
    means = np.random.uniform(0, 1, len(mab))
    one = np.ones(len(mab))

    # Pulling the arms to get to t_max
    while len(draws) < t_max:
        chos_arm = np.argmax(
            np.random.beta(s + one, n - s + one))  # the beta distribution

        reward = mab[chos_arm].sample()
        rew.append(reward)
        # Thompson for general stochastic bandit
        if reward != 1 and reward != 0:
            # If r is between 0 and 1, we do a Bernoulli trial with parameter reward
            reward = np.random.binomial(1, reward)

        draws.append(chos_arm)
        n[chos_arm] += 1
        s[chos_arm] += reward
        means[chos_arm] = s[chos_arm] / n[chos_arm]

    return rew, draws


def naive_strat(t_max, mab):
    """ Apply the Naive strategy algorithm to estimate the arms

    :param t_max: Number of iteration
    :param mab: Multi-arm bandit
    :return: rew: list of all the rewards
             draws : list of the pulled arms
    """
    # Initialisations
    rew = []
    draws = []
    n = np.zeros(len(mab))
    s = np.zeros(len(mab))
    means = np.ones(len(mab))

    # Pulling the arms to get to t_max
    while len(draws) < t_max:
        chos_arm = np.argmax(means)

        reward = mab[chos_arm].sample()
        draws.append(chos_arm)
        rew.append(reward)
        n[chos_arm] += 1
        s[chos_arm] += reward
        means[chos_arm] = s[chos_arm] / n[chos_arm]

    return rew, draws


def kl(x, y):
    """ Function used to compute the complexity

    :param x:
    :param y:
    :return: kl(x,y)
    """
    return x * np.log(x / y) + (1 - x) * np.log((1 - x) / (1 - y))


def complexity(list_p, p_opt=None):
    """ Compute the complexity of a Multi-arm bandit

    :param list_p: list of the probability
    :param p_opt: optimal probability (maximum)
    :return: C: complexity of the problem
    """
    if p_opt is None:
        p_opt = np.max(list_p)

    return np.sum([(p_opt - p_a) / kl(p_a, p_opt)
                   for p_a in list_p if p_a < p_opt])


def plot_algorithm(t, mab, naive=True, nb_simu=50):
    # bandit : set of arms
    means = [el.mean for el in mab]

    # Display the means of your bandit (to find the best)
    print('means: {}'.format(means))
    mu_max = np.max(means)

    # Comparison of the regret on 50 runs of the bandit algorithm
    # try to run this multiple times, you should observe different results

    rew1 = np.zeros(t)
    rew2 = np.zeros(t)
    rew3 = np.zeros(t)

    for _ in tqdm(range(nb_simu), desc="Simulating {}".format("Iterating SMAB")):
        # UCB1
        rew1_tp, _ = ucb1(t, mab)
        rew1 = rew1 + np.transpose(rew1_tp)
        # TS
        rew2_tp, _ = ts(t, mab)
        rew2 = rew2 + np.transpose(rew2_tp)
        # Naive Strat
        if naive:
            rew3_tp, _ = naive_strat(t, mab)
            rew3 = rew3 + np.transpose(rew3_tp)

    # We divide by the nb of simulations to get an average
    rew1 /= nb_simu
    rew2 /= nb_simu
    rew3 /= nb_simu

    #  We compute the regret
    reg1 = mu_max * np.arange(1, t + 1) - np.cumsum(rew1)
    reg2 = mu_max * np.arange(1, t + 1) - np.cumsum(rew2)
    reg3 = mu_max * np.arange(1, t + 1) - np.cumsum(rew3)

    # Complexity
    c_p = complexity([arm.mean for arm in mab])

    # We plot the figures
    plt.figure(1)
    x = np.arange(1, t + 1)
    plt.plot(x, reg1, label='UCB1')
    plt.plot(x, reg2, label='Thompson')
    if naive:
        plt.plot(x, reg3, label='Naive Strategy')
    plt.plot(x, c_p * np.log(x), label='Oracle regret curve')
    plt.xlabel('Rounds')
    plt.ylabel('Cumulative Regret')
    plt.legend()

    plt.show()
