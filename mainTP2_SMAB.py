import arms
import matplotlib.pyplot as plt
from MAB_algorithms import *

# Build your own bandit problem

# this is an example, please change the parameters or arms!
arm1 = arms.ArmBernoulli(0.30, random_state=np.random.randint(1, 312414))
arm2 = arms.ArmBernoulli(0.25, random_state=np.random.randint(1, 312414))
arm3 = arms.ArmBernoulli(0.20, random_state=np.random.randint(1, 312414))
arm4 = arms.ArmBernoulli(0.10, random_state=np.random.randint(1, 312414))

MAB = [arm1, arm2, arm3, arm4]
T = 5000

# plot_algorithm(T, MAB)

means2 = [0.90, 0.1, 0.1, 0.25]
MAB2 = [arms.ArmBernoulli(mean, random_state=np.random.randint(1, 312414))
       for mean in means2]
plot_algorithm(T, MAB2, False)

# TODO: Estimates average regret from many tries, in order to compare algorithms
