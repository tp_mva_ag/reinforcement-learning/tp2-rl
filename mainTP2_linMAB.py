from linearmab_models import *
import matplotlib.pyplot as plt
from tqdm import tqdm


##################################################################
# define the algorithms
# - Random
# - Linear UCB
# - Eps Greedy
# and test it!
##################################################################

def update_theta_hat(model, action, lambd, a_mtx, b_mtx):
    """ Compute the theta_hat and update a and b

    :param model: the model
    :param action: the chosen action
    :param lambd: lambda parameter
    :param a_mtx: the matrix A
    :param b_mtx: the B matrix
    :return:
            theta_hat: the estimator of theta
            rew_t: The reward got
    """
    rew_t = model.reward(action)  # get the reward
    phi_t = (np.array([model.features[action]])).transpose()

    # do something (update algorithm)
    a_mtx += np.dot(phi_t, phi_t.transpose())
    b_mtx += rew_t * phi_t

    theta_hat = np.dot(np.linalg.inv(a_mtx), b_mtx)

    return theta_hat, rew_t


def ucb_algo(p_model, a, theta_hat, alpha):
    n_a = p_model.n_actions

    list_value_a = []
    for act in range(n_a):
        phi_a = (np.array([p_model.features[act]])).transpose()

        prod = np.dot(
            np.dot(phi_a.transpose(), np.linalg.inv(a)),
            phi_a)
        beta_a = alpha * np.sqrt(prod)

        value = np.dot(phi_a.transpose(), theta_hat) + beta_a
        list_value_a.append(value)

    a_t = np.argmax(list_value_a)

    return a_t


def eps_greedy_algo(p_model, a, theta_hat, eps=0.5, alpha=2):
    if np.random.uniform(0, 1) < eps:
        # With proba eps, we pock a random action
        a_t = random_algo(p_model)
    else:
        # Else, we pick the best action (using the UCB choice method)
        a_t = ucb_algo(p_model, a, theta_hat, alpha)

    return a_t


def random_algo(p_model):
    n_a = p_model.n_actions

    a_t = int(np.random.uniform(0, n_a))  # algorithm picks the action randomly

    return a_t


def plot_algorithm_linMAB(model, t_max, nb_simu, alg_name, alpha=2, lambd=0.01, eps=0.5, plot_figures=False):
    d = model.n_features

    regret = np.zeros((nb_simu, t_max))
    norm_dist = np.zeros((nb_simu, t_max))

    mean_theta_hat = np.zeros((d,1))

    for k in tqdm(range(nb_simu), desc="Simulating {}".format(alg_name)):

        a_mtx = lambd * np.eye(d)
        b_mtx = np.zeros((d, 1))

        theta_hat = np.ones(d)

        for t in range(t_max):

            if alg_name == "UCB" or alg_name == "linUCB":
                a_t = ucb_algo(model, a_mtx, theta_hat, alpha)
            elif alg_name == "eps":
                a_t = eps_greedy_algo(model, a_mtx, theta_hat, eps)
            else:
                a_t = random_algo(model)

            theta_hat, rew_t = update_theta_hat(model, a_t, lambd, a_mtx, b_mtx)

            # store regret
            regret[k, t] = model.best_arm_reward() - rew_t
            norm_dist[k, t] = np.linalg.norm(theta_hat.transpose() - model.real_theta, 2)

        mean_theta_hat = mean_theta_hat + (theta_hat / nb_simu)

    # compute average (over sim) of the algorithm performance and plot it
    mean_norms = [np.mean(norm_dist[:, t]) for t in range(t_max)]
    mean_regret = [np.mean(regret[:, t]) for t in range(t_max)]

    if plot_figures:
        plt.figure()
        plt.subplot(121)
        plt.plot(mean_norms, label=alg_name)
        plt.ylabel('d(theta, theta_hat)')
        plt.xlabel('Rounds')
        plt.legend()

        plt.subplot(122)
        plt.plot(np.cumsum(mean_regret), label=alg_name)
        plt.ylabel('Cumulative Regret')
        plt.xlabel('Rounds')
        plt.legend()

    return mean_theta_hat, mean_norms, mean_regret
