from mainTP2_linMAB import *

random_state = np.random.randint(0, 24532523)

model = ToyLinearModel(
    n_features=8,
    n_actions=20,
    random_state=random_state,
    noise=0.1)

# model = ColdStartMovieLensModel(
#     random_state=random_state,
#     noise=0.1
# )

T = 6000
nb_simu = 50  # you may want to change this!

theta_ucb, mean_norms_ucb, mean_regret_ucb = plot_algorithm_linMAB(model, T, nb_simu, "linUCB", lambd=1)
theta_eps, mean_norms_eps, mean_regret_eps = plot_algorithm_linMAB(model, T, nb_simu, "eps", lambd=1)
theta_rand, mean_norms_rand, mean_regret_rand = plot_algorithm_linMAB(model, T, nb_simu, "random", lambd=1)

plt.figure()
plt.subplot(121)
plt.plot(mean_norms_ucb, label='UCBlin')
plt.plot(mean_norms_eps, label='greedy')
plt.plot(mean_norms_rand, label='random')
plt.ylabel('d(theta, theta_hat)')
plt.xlabel('Rounds')
plt.legend()

plt.subplot(122)
plt.plot(np.cumsum(mean_regret_ucb), label='UCB')
plt.plot(np.cumsum(mean_regret_eps), label='greedy')
plt.plot(np.cumsum(mean_regret_rand), label='random')
plt.ylabel('Cumulative Regret')
plt.xlabel('Rounds')
plt.legend()

plt.show()

